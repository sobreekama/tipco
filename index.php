<!doctype html>
<html lang="en">
 <head>
  <meta charset="UTF-8">
  <meta name="Generator" content="EditPlus®">
  <meta name="Author" content="">
  <meta name="Keywords" content="">
  <meta name="Description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <!--Import Google Icon Font -->
  <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <!--Import materialize.css -->
  <link type="text/css" rel="stylesheet" href="css/materialize.css"  media="screen,projection"/>
  <title>Tipco</title>
 </head>
 <body>
   <div class="nav-fixed">
    <nav class="container lighten-1" role="navigation">
      <div class="nav-wrapper">
        <!-- <a href="#" class="brand-logo"><img src="image/logo.png" class="img-logo"/></a> -->
        <!-- <a id="logo-container" href="#" class="brand-logo">Logo</a> -->
        <ul class="hide-on-med-and-down">
          <!-- <li><a href="#!"><img src="image/logo.png" class="img-logo"/></a></li> -->
          <li><a href="#!">HOME</a></li>
          <li><a href="#!">PRODUCT</a></li>
          <li><a href="#!">GAME</a></li>
          <li><a href="#!">FAQs</a></li>
          <li><a href="#!">CONTACT us</a></li>
          <li><input id="search" name="search" placeholder="Search.." type="text"></li>
          <li>
            <a href="#!" class="nav-img"><img src="image/icon1.png" class="img-icon"/></a>
            <!-- <a href="#!"><img src="image/icon2.png" class="img-icon"/></a>
            <a href="#!"><img src="image/icon3.png" class="img-icon"/></a> -->
            <a href="#!"><img src="image/icon4.png" class="img-icon"/></a>
          </li>
        </ul>
        <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
      </div>
    </nav>
  </div>
  <ul id="nav-mobile" class="side-nav" style="transform: translateX(-100%);">
    <li><a href="#!">HME</a></li>
    <li><a href="#!">PRODUCT</a></li>
    <li><a href="#!">GAME</a></li>
    <li><a href="#!">FAQs</a></li>
    <li><a href="#!">CONTACT us</a></li>
  </ul>


<div id="HOME" class="block">
    <div class="container">
    <div class="row">
      <div class="col l6"><img src="image/wave1.png" class="full"/></div>
      <div class="col l6"><img src="image/video.jpg" class="full"/></div>
      <div class="col l12"><img src="image/wave2.png" class="full" style="padding: 5%;"/></div>
    </div><!-- .row -->
</div><!-- .container -->
</div><!-- .HOME -->


<div id="red" class="block red lighten-1">
</div>

<!-- Import jQuery before materialize.js -->
<script type="text/javascript" src="js/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="js/materialize.min.js"></script>
<script>(function($){
  $(function(){
    $('.button-collapse').sideNav();
    }); // end of document ready
  })(jQuery); // end of jQuery name space
</script>
 </body>
</html>
